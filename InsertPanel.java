import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class InsertPanel extends JFrame {
    private JPanel insertPanel;
    private JTextField word;
    private JTextField meaning;
    private JTextField pronunciation;
    private JComboBox genreComboBox;
    private JButton insertButton;
    private JLabel genreLabel;
    private JLabel meaningLabel;
    private JLabel pronunciationLabel;
    private JLabel inform;
    private JLabel wordLabel;
    private JButton exitButton;
    private Word newWord = new Word();
    private StringBuilder word_explain = new StringBuilder();


    public InsertPanel(DictionaryManagement dictionary) {
        setTitle("Insert word to dictionary");
        this.setContentPane(insertPanel);
        this.pack();
        setVisible(true);

        insertButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                int out = JOptionPane.showConfirmDialog(insertPanel,"Are you sure to insert this word?", "Inform", JOptionPane.YES_NO_OPTION);
                if (out == JOptionPane.YES_NO_OPTION) {
                    newWord.setWord_target(word.getText().toLowerCase());
                    word_explain.append("(" + genreComboBox.getSelectedItem().toString() + ")" + "\t" + 
                            pronunciation.getText().toLowerCase() + "\t" + meaning.getText().toLowerCase());
                    newWord.setWord_explain(word_explain.toString());
                    if (dictionary.dictionaryLookup(newWord.getWord_target()).equals("Have no such word.")) {
                        dictionary.getListWord().add(newWord);
                        dictionary.dictionaryExportToFile();
                    }
                }
                word_explain.delete(0, word_explain.length());
            }
        });

        genreComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {}
        });


        pronunciation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {}
        });
        word.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {}
        });
        meaning.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {}
        });
        exitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                dispose();
            }
        });
    }

    public static void main(String[] args) {
        DictionaryManagement dict = new DictionaryManagement();
        EventQueue.invokeLater(() -> {
            InsertPanel insertFrame = new InsertPanel(dict);
        });
    }
}
