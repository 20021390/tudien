import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class DictionaryApplication extends javax.swing.JFrame {
    private JPanel mainPanel;
    private JTextField keySearch;
    private JLabel label1;
    private JLabel label2;
    private JButton searchButton;
    private JScrollPane scroll1;
    private JButton meaningButton;
    private JTextArea explainPanel;
    private JList list1;
    private JMenuBar menuBar;

    public void menuSetup(DictionaryManagement dictionary) {
        menuBar = new JMenuBar();
        JMenu homeMenu = new JMenu("Home");
        JMenu actionMenu = new JMenu("Menu");
        JMenu exitAction = new JMenu("Exit");

        JMenuItem insertAction = new JMenuItem("Insert");
        JMenuItem deleteAction = new JMenuItem("Delete");
        actionMenu.add(insertAction);
        actionMenu.add(deleteAction);

        exitAction.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                System.exit(1);
            }
        });

        insertAction.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("insert");
                EventQueue.invokeLater(() -> {
                    InsertPanel insertFrame = new InsertPanel(dictionary);
                });

            }
        });

        deleteAction.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("delete");
                EventQueue.invokeLater(() -> {
                    DeletePanel deleteFrame = new DeletePanel(dictionary);
                });
            }
        });
        menuBar.add(homeMenu);
        menuBar.add(actionMenu);
        menuBar.add(exitAction);
    }

    private DictionaryApplication(DictionaryManagement dictionary) {
        setTitle("English - Vietnamese Dictionary");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(mainPanel);
        this.pack();


        Vector<String> listWord_target = dictionary.getListWord_target();
        list1.setListData(listWord_target);

        menuSetup(dictionary);
        setJMenuBar(menuBar);

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String word = keySearch.getText();
                list1.setListData(dictionary.dictionarySearcher(word.toLowerCase()));

            }
        });

        list1.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!list1.isSelectionEmpty()) {
                    String word = (String) list1.getSelectedValue();
                    explainPanel.setText(dictionary.dictionaryLookup(word));
                }
            }
        });

    }


    public static void main(String[] args) {
        DictionaryManagement dictionary = new DictionaryManagement();
        dictionary.insertFromFile();
        EventQueue.invokeLater(() -> {
            JFrame frame = new DictionaryApplication(dictionary);
            frame.setVisible(true);
        });
    }
}
