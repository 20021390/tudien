import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

public class DictionaryCommandLine extends DictionaryManagement{

    DictionaryCommandLine() {
        super();
    }

    public void showAllWords() {
        System.out.printf("%-5s %-20s %s %n", "No", "| English", "| Vietnamese");
        int i = 1;
        for(Word w : super.getListWord()) {
            System.out.printf("%-5s %-20s %s %n", i,
                    "| " + w.getWord_target(), "| " + w.getWord_explain());
            i++;
        }
    }

    public void dictionaryBasic() {
        super.insertFromCommandLine();
        showAllWords();
    }

    public void dictionaryAdvanced() {
        super.insertFromFile();
        showAllWords();

    }




}
