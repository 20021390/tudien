import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.*;

public class DictionaryManagement extends Dictionary{

    public void insertFromCommandLine() {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the word you wanna insert to dictionary: ");
        String word = input.nextLine();
        System.out.print("Enter the meaning of the word: ");
        String meaning = input.nextLine();
        Word newWord = new Word(word, meaning);
        super.getListWord().add(newWord);
    }

    public void insertFromFile() {
        Scanner input = null;
        try {
            input = new Scanner(new File("H:\\JAVA\\PROJECT1_DICTIONARY_VER2\\src\\dictionaries.txt"));
        } catch (FileNotFoundException fileNotFoundException) {
            System.err.println("Error opening file.");
            System.exit(1);
        }

        try {
            while (input.hasNext()) {
                String curLine = input.nextLine();
                String[] splitted = curLine.split("\t", 4);
                String word = splitted[0].trim().toLowerCase();
                String meaning = splitted[1].trim() + "\t" + splitted[2].trim() + "\t" + splitted[3].trim();
                Word newWord = new Word(word, meaning);
                super.getListWord().add(newWord);

            }
        } catch (NoSuchElementException elementException) {
            System.err.println("File improperly formed.");
            input.close();System.exit(1);
        } catch (IllegalStateException stateException) {
            System.err.println("Error reading from file");
            System.exit(1);
        }
        if (input != null) {
            input.close();
        }
    }

    public String dictionaryLookup(String word) {
        for(Word w : super.getListWord()) {
            if (word.equals(w.getWord_target().toLowerCase())) {
                return w.getWord_explain();
            }
        }
        return "Have no such word.";
    }

    public void editDictionary() {
        Scanner input = new Scanner(System.in);
        System.out.println("Which word you want to edit?: ");
        String word = input.nextLine();
        for (Word w : super.getListWord()) {
            if(word.equals(w.getWord_target())) {
                System.out.print("Enter the word you want to change into: ");
                String newWord = input.nextLine();
                w.setWord_target(newWord);
                System.out.print("Enter new meaning of this word: ");
                String meaning = input.nextLine();
                w.setWord_explain(meaning);
                return;
            }
        }
        System.out.println("Have no such this word.");
    }

    public String deleteWord(String word) {
        for(Word w : super.getListWord()) {
            if(w.getWord_target().equals(word.toLowerCase())) {
                super.getListWord().remove(w);
                return "Done!!!";
            }
        }
        return "Have no such word.";
    }

    public void dictionaryExportToFile() {
        Formatter output = null;
        try {
            output = new Formatter("H:\\JAVA\\PROJECT1_DICTIONARY_VER2\\src\\dictionaries.txt");
        } catch (SecurityException securityException) {
            System.err.println("You do not have write access to this file");
            System.exit(1);
        } catch (FileNotFoundException fileNotFoundException) {
            System.err.println("Error opening or create file");
            System.exit(1);
        }
        for (Word w : super.getListWord()) {
            try {
                output.format("%s\t %s\n", w.getWord_target(), w.getWord_explain());
            } catch (FormatterClosedException  formatterClosedException) {
                System.err.println("Error writing to file");
                return;
            } catch (NoSuchElementException elementException) {
                System.err.println("Invalid input.");
                return;
            }
        }
        if (output != null) {
            output.close();
        }
    }

    public Vector<String> dictionarySearcher(String word) {
        Vector<String> record = new Vector<>();
        for(Word w : super.getListWord()) {
            if(w.getWord_target().length() >= word.length() &&
                    w.getWord_target().substring(0, word.length()).equals(word)) {
                    record.add(w.getWord_target());
            }
        }
        if(record.size() == 0) {
            record.add("Have no such word.");
        }
        return record;
    }
}
