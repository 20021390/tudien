import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public abstract class Dictionary {
    private ArrayList<Word> listWord = new ArrayList<Word>();
    private int size;

    public ArrayList<Word> getListWord() {
        return listWord;
    }

    public void setListWord(ArrayList<Word> listWord) {
        this.listWord = listWord;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Dictionary() {
        size = 0;
    }

    public Vector<String> getListWord_target() {
        Vector<String> targetWords = new Vector<>();
        for(Word w : listWord) {
            targetWords.add(w.getWord_target());
        }
        return targetWords;
    }

}
