import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DeletePanel extends JFrame{
    private JPanel deletePanel;
    private JTextField word;
    private JButton enterButton;
    private JLabel label;
    private JTextField inform;
    private JButton exitButton;
    private String word_target;

    public DeletePanel(DictionaryManagement dictionary) {
        setTitle("Delete word from dictionary");
        this.setContentPane(deletePanel);
        this.pack();
        setVisible(true);
        word.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                word_target = word.getText();
                System.out.println(word_target);
            }
        });
        enterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String info = dictionary.deleteWord(word_target);
                int out = JOptionPane.showConfirmDialog(deletePanel,"Are you sure to delete this word?", "Inform", JOptionPane.YES_NO_OPTION);
                if (out == JOptionPane.YES_NO_OPTION) {
                    inform.setText(info);
                    dictionary.dictionaryExportToFile();
                    System.out.println(inform.getText());
                }
            }
        });
        exitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                dispose();
            }
        });
    }

    public static void main(String[] args) {
        DictionaryManagement dict = new DictionaryManagement();

        EventQueue.invokeLater(() -> {
            DeletePanel frame = new DeletePanel(dict);
        });
    }
}
